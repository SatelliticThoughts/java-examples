import java.awt.*;
import javax.swing.border.*;
import javax.swing.*;

public class Gui {
    public static void main(String[] args) {
        Window win = new Window();
        win.setVisible(true);
    }
}

class Window extends JFrame {
    private JTextArea txtArea;
    private JTextField txtField;

    public Window() {
        createGui();
    }

    public void createGui() {
        setTitle("My Window");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel pMain = new JPanel();
        pMain.setBorder(new EmptyBorder(10, 10, 10, 10));
        BoxLayout yBox = new BoxLayout(pMain, BoxLayout.Y_AXIS);
        pMain.setLayout(yBox);

        JPanel pLbl = new JPanel();
        pLbl.setLayout(new FlowLayout());
        pLbl.add(new JLabel("Hello"));
        pLbl.add(new JLabel("World"));
        pMain.add(pLbl);

        JPanel pField = new JPanel();
        pField.setLayout(new FlowLayout());
        txtField = new JTextField(25);
        pField.add(txtField);
        pMain.add(pField);

        txtArea = new JTextArea(100, 0);
        JScrollPane scroll = new JScrollPane(txtArea);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
 
        pMain.add(scroll);

        JPanel pBtn = new JPanel();
        pBtn.setLayout(new FlowLayout());
        JButton btnHello = new JButton("Hello");
        btnHello.addActionListener(e -> onHelloClicked());
        pBtn.add(btnHello);
        JButton btnBye = new JButton("Bye");
        btnBye.addActionListener(e -> onByeClicked());
        pBtn.add(btnBye);
        pMain.add(pBtn);

        add(pMain);
        pack();
        setSize(600, 400);
    }

    public void onHelloClicked() {
        txtField.setText("Hello");
        txtArea.append("Hello" + "\n");
    }

    public void onByeClicked() {
        txtField.setText("Bye");
        txtArea.append("Bye" + "\n");
    }
}