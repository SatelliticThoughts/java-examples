public class Insertion 
{
	public static void main(String[] args)
	{
		System.out.printf("\nInsertion Sort\n\n\tArray to be sorted\n\t\t");
		
		int[] numbers = {2,5,1,7,8,9,0,1,2};
		
		for(int i = 0; i < numbers.length; i++) 
		{
			System.out.printf("%d, ", numbers[i]);
		}
		
		insertionSort(numbers);
		
		System.out.printf("\n\n\tArray Sorted\n\t\t");
		for(int i = 0; i < numbers.length; i++)
		{
			System.out.printf("%d, ", numbers[i]);
		}
		
		System.out.println();
	}
	
	public static void insertionSort(int[] numbers)
	{
		int checks = 0;
		
		for(int i = 1; i < numbers.length; i++)
		{
			checks++;
			
			int key = numbers[i];
			int j = i - 1;
			
			while(j >= 0 && numbers[j] > key)
			{
				checks++;
				numbers[j + 1] = numbers[j];
				j--;
			}
			numbers[j + 1] = key;
		}
		System.out.printf("\n\n\tChecks: %d", checks);
	}
}
