public class Bubble
{
	public static void main(String[] args)
	{
		System.out.println("\nBubble sort\n");
		
		int[] numbers = {2,5,1,4,7,8,9,0,9,8,6,3};
		System.out.println("Before sort");
		for(int i = 0; i < numbers.length; i++)
		{
			System.out.printf("%d, ", numbers[i]);
		}
		
		bubbleSort(numbers);
		System.out.println("\n\nAfter sort");
		for(int i = 0; i < numbers.length; i++)
		{
			System.out.printf("%d, ", numbers[i]);
		}
		System.out.print("\n");
	}
	
	public static void bubbleSort(int[] array)
	{
		int checks = 0;
		for(int i = 0; i < array.length - 1; i++)
		{
			checks++;
			for(int j = 0; j < array.length - i - 1; j++)
			{
				checks++;
				if(array[j] > array[j + 1])
				{
					int temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
		System.out.printf("\n\nChecks: %d", checks);
	}
}
