public class Merge
{
	public static void main(String[] args)
	{
		System.out.println("\nMerge sort\n");
		
		int[] numbers = {2,5,2,1,7,8,5,3,6,8,5,9,0};
		System.out.println("Before sort");
		for(int i = 0; i < numbers.length; i++)
		{
			System.out.printf("%d, ", numbers[i]);
		}
		
		mergeSort(numbers, 0, numbers.length - 1);
		System.out.println("\n\nAfter sort");
		for(int i = 0; i < numbers.length; i++)
		{
			System.out.printf("%d, ", numbers[i]);
		}
		System.out.println("\n");
	}
	
	public static void mergeSort
			(
				int[] array, 
				int left, 
				int right
			)
	{
		if(left < right)
		{
			int middle = (left + right) / 2;
			mergeSort(array, left, middle);
			mergeSort(array, middle + 1, right);
			
			merge(array, left, middle, right);
		}
	}
	
	public static void merge
			(
				int[] array, 
				int left, 
				int middle, 
				int right
			)
	{
		int leftLength = middle - left + 1;
		int rightLength = right - middle;
		
		int[] leftTemp = new int[leftLength];
		int[] rightTemp = new int[rightLength];
		
		for(int i = 0; i < leftLength; i++)
		{
			leftTemp[i] = array[left + i];
		}
		for(int i = 0; i < rightLength; i++)
		{
			rightTemp[i] = array[middle + 1 + i];
		}
		
		int i = 0;
		int j = 0;
		int k = left;
		
		while(i < leftLength && j < rightLength)
		{
			if(leftTemp[i] <= rightTemp[j])
			{
				array[k] = leftTemp[i];
				i++;
			}
			else
			{
				array[k] = rightTemp[j];
				j++;
			}
			k++;
		}
		
		while(i < leftLength)
		{
			array[k] = leftTemp[i];
			i++;
			k++;
		}
		
		while(j < rightLength)
		{
			array[k] = rightTemp[j];
			j++;
			k++;
		}
	}
}
