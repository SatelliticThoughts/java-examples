public class Quick
{
	public static void main(String[] args)
	{
		System.out.println("\nQuick sort\n");
		
		int[] numbers = {2,1,4,6,7,5,3,2,5,7};
		System.out.println("Before sort");
		for(int i = 0; i < numbers.length; i++)
		{
			System.out.printf("%d, ", numbers[i]);
		}
		
		quickSort(numbers, 0, numbers.length - 1);
		System.out.println("\n\nAfter sort");
		for(int i = 0; i < numbers.length; i++)
		{
			System.out.printf("%d, ", numbers[i]);
		}
		System.out.println("\n");
	}
	
	public static void quickSort
			(
				int[] array, 
				int low, 
				int high
			)
	{
		if(low < high)
		{
			int partitionIndex = partition(array, low, high);
			quickSort(array, low, partitionIndex - 1);
			quickSort(array, partitionIndex + 1, high); 
		}
	}
	
	public static int partition
			(
				int[] array, 
				int low, 
				int high
			)
	{
		int pivot = array[high];
		int smallestIndex = low - 1;
		for(int i = low; i < high; i++)
		{
			if(array[i] < pivot)
			{
				smallestIndex++;
				int temp = array[smallestIndex];
				array[smallestIndex] = array[i];
				array[i] = temp;
			}
		}
		int temp = array[smallestIndex + 1];
		array[smallestIndex + 1] = array[high];
		array[high] = temp;
		
		return smallestIndex + 1;
	}
}
