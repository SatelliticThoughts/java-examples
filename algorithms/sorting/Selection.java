public class Selection
{
	public static void main(String[] args)
	{
		System.out.println("\nSelection sort\n\nBefore sort");
		
		int[] numbers = {2,4,6,1,2,8,9,0,1};
		for(int i = 0; i < numbers.length; i++)
		{
			System.out.printf("%d, ", numbers[i]);
		}
		
		selectionSort(numbers);
		
		System.out.println("\n\nAfter sort");
		
		for(int i = 0; i < numbers.length; i++)
		{
			System.out.printf("%d, ", numbers[i]);
		}
		
		System.out.println("\n");
	}
	
	public static void selectionSort(int[] array)
	{
		int checks = 0;
		for(int i = 0; i < array.length; i++)
		{
			checks++;
			int indexMin = i;
			for(int j = i + 1; j < array.length; j++)
			{
				checks++;
				if(array[j] < array[indexMin])
				{
					indexMin = j;
				}
			}
			int temp = array[i];
			array[i] = array[indexMin];
			array[indexMin] = temp;
		}
		System.out.printf("\n\n%d", checks);
	}
}
