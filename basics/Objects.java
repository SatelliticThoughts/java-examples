public class Objects
{
	public static void main(String[] args)
	{
		Person p = new Person("luke", 103); // Calling Constructor

		// Calling getters
		System.out.println("Name: " + p.getName() + 
				" Age: " + p.getAge());
	}
}

class Person
{
	private String name; // Property
	private int age; // Property

	// Constructor
	public Person(String name, int age)
	{
		this.setName(name); // Calling setter
		this.setAge(age); // Calling setter
	}

	// Getter
	public String getName()
	{
		return this.name;
	}

	// Setter
	public void setName(String name)
	{
		this.name = name;
	}

	// Getter
	public int getAge()
	{
		return this.age;
	}

	// Setter
	public void setAge(int age)
	{
		this.age = age;
	}
}

