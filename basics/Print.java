public class Print 
{
	public static void main(String[] args)
	{
		// Hello world example
		System.out.println("Hello World!");

		// Printing variable example
		String text = "This is a test";
		System.out.println(text);

		// Printing multiple variables
		String name = "luke";
		int age = 92;
		System.out.println("My name is " + name + 
				" and i am " + age + " years old");
	}
}

