public class Decisions
{
	public static void main(String[] args)
	{
		// Basic if statement.
		if(true)
		{
			System.out.println("This will show");
		}

		// Basic if/else statement.
		if(false)
		{
			System.out.println("This will NOT show");
		}
		else
		{
			System.out.println("This will show");
		}

		// Basic if/else if/else statement
		if(false)
		{
			System.out.println("This will NOT show");
		}
		else if(true)
		{
			System.out.println("This will show");
		}
		else 
		{
			System.out.println("This will NOT show");
		}

		// &&, and
		if(true && true)
		{
			System.out.println("This will show");
		}

		if(true && false)
		{
			System.out.println("This will NOT show");
		}

		if(false && false)
		{
			System.out.println("This will NOT show");
		}
		
		// ||, or
		if(true || true)
		{
			System.out.println("This will show");
		}

		if(true || false)
		{
			System.out.println("This will show");
		}

		if(false || false)
		{
			System.out.println("This will NOT show");
		}

		int x = 45;
		int y = 32;

		// Equals
		if(x == y)
		{
			System.out.println("If x equals y this will show");
		}

		// Not Equal
		if(x != y)
		{
			System.out.println(
				"If x does not equal y this will show");
		}

		// Greater Than
		if(x > y)
		{
			System.out.println(
				"If x is bigger than y this will show");
		}

		// Less Than
		if(x < y)
		{
			System.out.println(
				"If x is smaller than y this will show");
		}
	}
}

