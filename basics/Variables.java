public class Variables
{
	public static void main(String[] args)
	{
		// Integers
		int x = 23;
		int y = 165;
		System.out.println("Integer Calculations, x = " + x + 
				", y = " + y);

		System.out.println("x + y = " + (x + y)); // Addition
		System.out.println("x - y = " + (x - y)); // Subtraction
		System.out.println("x * y = " + (x * y)); // Multiplication
		System.out.println("x / y = " + (x / y)); // Division
		System.out.println("x % y = " + (x % y)); // Modulus

		// Floating point numbers
		float a = 13.5287f;
		float b = 98.729f;
		System.out.println("\nFloating point calculations, a = " + a +
				", b = " + b);

		System.out.println("a + b = " + (a + b)); // Addition
		System.out.println("a - b = " + (a - b)); // Subtraction
		System.out.println("a * b = " + (a * b)); // Multiplication
		System.out.println("a / b = " + (a / b)); // Division
		System.out.println("a % b = " + (a % b)); // Modulus

		// Strings
		System.out.println("\nString Manipulation");

		String hello = "Hello";
		System.out.println(hello);

		hello = hello + " world ";
		System.out.println(hello); // String addition

		System.out.println(hello.substring(4, 8)); // Substring

		// Arrays
		int[] numbers = new int[10];
	       	for(int i = 0; i < numbers.length; i++)
		{
			numbers[i] = i * 5;
		}

		System.out.println(numbers[7]);
	}
}

