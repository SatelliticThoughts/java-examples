public class Loops
{
	public static void main(String[] args)
	{
		boolean running = true;
		int num = 0;

		// while loop
		while(running)
		{
			num += 1;
			if(num > 5)
			{
				running = false;
			}
		}

		// for loop
		for(int i = 1; i < 13; i++)
		{
			System.out.println("128 / " + i + " = " + 128/i);
		}

		double[] numbers = {2.3, 4.76, 56.12, 8, 1442.670393};
		 
		// foreach loop
		for(double n: numbers)
		{
			System.out.println(n);
		}
	}
}

