# java-examples

Java-examples contains code to demonstrate various things in java.

## Set up

After cloning or downloading, most programs should be a single file that can be compiled with javac and run with java, unless otherwise stated.

```
javac 'filename.java' # compile file
java 'filename' # run file
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
